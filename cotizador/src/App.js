import { useState } from 'react';
import Header from './Components/Header/Header';
import Formulario from './Components/Formulario/Formulario';
import Resumen from './Components/Resumen/Resumen';
import Resultado from './Components/Resultado/Resultado';
import Spinner from './Components/Spinner/Spinner';
import styled from '@emotion/styled';

const Contenedor = styled.div`
  max-width: 600px;
  margin: 0 auto;
`;

const ContenedorFormulario = styled.div`
  background-color: #FFF;
  padding: 3rem;
`;


function App() {

  const [resumen, guardarResumen] = useState({
    cotizacion: 0,
    datos: {
      marca: '',
      year: '',
      plan: ''
    }
  });

  const [spinner, guardarSpinner] = useState(false);

  const { datos, cotizacion } = resumen;

  return (
    <Contenedor>
      <Header
        titulo='Cotizador de seguros'
      />

      <ContenedorFormulario>
        <Formulario
          guardarResumen={guardarResumen}
          guardarSpinner={guardarSpinner}
        />

        <Resumen
          datos={datos}
        />

        {spinner ? <Spinner /> : null}


        {!spinner ?
          <Resultado
            cotizacion={cotizacion}
          />
          : null}

      </ContenedorFormulario>
    </Contenedor>
  );
}

export default App;
