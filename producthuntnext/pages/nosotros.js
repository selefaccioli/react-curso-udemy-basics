import React from 'react';
import styled from '@emotion/styled';
import Layout from '../components/layout/Layout';

export default function Home() {
    return (
        <Layout>
            <h1>Nosotros</h1>
        </Layout>
    )
}
